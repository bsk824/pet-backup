class DatePicker {
  constructor(params) {
    this.wrap = (typeof params.wrap === 'string') ? document.querySelector(`${params.wrap}`) : params.wrap;
    this.type = (params.type) ? params.type : null;
    this.prevShowLine = (params.prevShowLine) ? params.prevShowLine : 0;
    this.nextShowLine = (params.nextShowLine) ? params.nextShowLine : 0;
    this.minDate = (params.minDate) ? params.minDate : null;
    this.maxDate = (params.maxDate) ? params.maxDate : null;
    this.days = ['일','월','화','수','목','금','토'];
    this.selectValue = null;
    this.periodValue = {start: null, end: null};
    this.currentYear = this.getNow().getFullYear();
    this.currentMonth = this.getNow().getMonth();
    this.dateArry = [];
    this.dateWrap = null;
    this.makeDateArry();
  }
  getNow() {
    const date = new Date();
    const utc = date.getTime() + (date.getTimezoneOffset() * 60 * 1000);
    const kstGap = 9 * 60 * 60 * 1000;
    const now = new Date(utc + kstGap);
    return now;
  }
  prevMonth() {
    if(this.currentMonth === 0) {
      this.currentYear -= 1;
      this.currentMonth = 11;
    } else {
      this.currentMonth -= 1;
    }
  }
  nextMonth() {
    if(this.currentMonth === 11) {
      this.currentYear += 1;
      this.currentMonth = 0;
    } else {
      this.currentMonth += 1;
    }
  }
  makeDateArry() {
    this.dateArry = [];
    const todayStr = `${this.getNow().getFullYear()}.${String(this.getNow().getMonth() + 1).padStart(2, '0')}.${String(this.getNow().getDate()).padStart(2, '0')}`;
    
    const arryPushFunc = (dateStr, num, type) => {
      this.dateArry.push({
        date: new Date(dateStr),
        dateNum: num,
        monthType: type,
        today : (todayStr === dateStr) ? true : false,
        select : (this.selectValue === dateStr) ? true : false,
        periodStart: (this.periodValue.start === dateStr) ? true : false,
        periodEnd: (this.periodValue.end === dateStr) ? true : false,
        periodCurrent: (this.periodValue.start < dateStr && dateStr < this.periodValue.end) ? true : false,
        disabled: (this.minDate >= dateStr || dateStr >= this.maxDate) ? true : false,
      })
    }

    const startDay = new Date(this.currentYear, this.currentMonth, 0);
    const prevDay = startDay.getDay();
    const prevDate = startDay.getDate();
    const showPrevDay = prevDate - (this.prevShowLine * 7) - prevDay;
    
    const endDay = new Date(this.currentYear, this.currentMonth + 1, 0);
    const nextDate = endDay.getDate();

    const nextDay = endDay.getDay();
    let showNextDay = (6 - nextDay === 6) ? 6 : 6 - nextDay;
    showNextDay = (showNextDay === 0) ? 7 : showNextDay;
    showNextDay += this.nextShowLine * 7;

    for (let prevDayNum = showPrevDay; prevDayNum <= prevDate; prevDayNum++) {
      let prevYear = this.currentYear;
      let prevMonth = this.currentMonth - 1;
      if(prevMonth < 0) {
        prevMonth = 11;
        prevYear = this.currentYear - 1;
      }
      prevMonth = String(prevMonth + 1).padStart(2, '0');
      prevDayNum = String(prevDayNum).padStart(2, '0');

      let dateStr = `${prevYear}.${prevMonth}.${prevDayNum}`;
      arryPushFunc(dateStr, prevDayNum, 'prev');
    }

    for (let days = 1; days <= nextDate; days++) {
      let year = this.currentYear;
      let month = this.currentMonth;
      month = String(month + 1).padStart(2, '0');
      days = String(days).padStart(2, '0');

      let dateStr = `${year}.${month}.${days}`;
      arryPushFunc(dateStr, days, 'current');
    }

    for (let nextDayNum = 1; nextDayNum <= showNextDay; nextDayNum++) {
      let nextYear = this.currentYear;
      let nextMonth = this.currentMonth + 1;
      if(nextMonth > 11) {
        nextMonth = 0;
        nextYear = this.currentYear + 1;
      }
      nextMonth = String(nextMonth + 1).padStart(2, '0');
      nextDayNum = String(nextDayNum).padStart(2, '0');

      let dateStr = `${nextYear}.${nextMonth}.${nextDayNum}`;
      arryPushFunc(dateStr, nextDayNum, 'next');
    }
  }
  createDay() {
    let els = '';
    this.days.map(el=>{
      els += 
      `<div${(el == '일') ? ' class="sun"' : (el == '토') ? ' class="sat"' : ''}>${el}</div>`
    });
    return els;
  }
  createDate() {
    this.dateArry.map(el=>{
      const div = document.createElement('div');
      const btn = document.createElement('button');
      if(el.today) div.classList.add('isToday');
      if(el.select) div.classList.add('isSelect');
      if(el.disabled) {
        div.classList.add('isDisabled');
        btn.disabled = true;
      }
      if(el.periodStart) div.classList.add('isPeriodStart');
      if(el.periodEnd) div.classList.add('isPeriodEnd');
      if(el.periodCurrent) div.classList.add('isPeriodCurrent');
      
      btn.insertAdjacentHTML('beforeend', `<span>${el.dateNum}</span>`);
      switch(el.date.getDay()) {
        case 0 : {
          div.classList.add('sun');
          break;
        }
        case 6 : {
          div.classList.add('sat');
          break;
        }
      }
      div.insertAdjacentElement('beforeend', btn);
      btn.addEventListener('click',el=>{
        console.log(el);
      });
      this.dateWrap.insertAdjacentElement('beforeend', div);
    });
  }
  createWrap() {
    const wrap = document.createElement('div');
    wrap.classList.add('date-picker__wrap');
    wrap.insertAdjacentHTML('beforeend', `
      <div class="date-picker__head">
        <button class="date-picker--btn-prev" data-ref="calendarBtnPrev">이전</button>
        <div class="date-picker__title">
          <span class="date-picker--year" data-ref="calendarYear"></span>
          <span class="date-picker--month" data-ref="calendarMonth"></span>
        </div>
        <button class="date-picker--btn-next" data-ref="calendarBtnNext">다음</button>
      </div>
      <div class="date-picker__body">
        <div class="date-picker__days-wrap">
          ${this.createDay()}
        </div>
        <div class="date-picker__date-wrap" data-ref="calendarDateWrap"></div>
      </div>
    `);
    this.dateWrap = wrap.querySelector('[data-ref="calendarDateWrap"]');
    this.createDate();
    this.wrap.insertAdjacentElement('beforeend', wrap);
  }
}
